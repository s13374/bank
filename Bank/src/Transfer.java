public class Transfer extends Operation {

		private Account account;
		private Account accountTo;
		private double amount;
		
		public Transfer (Account accountTo, double amount) {
			this.accountTo = accountTo;
			this.amount = amount;
		}
		
		public void execute (Account account) {
			System.out.println("[" + account.getNumber() + "] przesyla " + amount + " zl do [" + accountTo.getNumber() + "]");
			account.subtact(amount);
			accountTo.add(amount);
		}
}