public class Test {

		public static void main (String args[]) {
			Bank bank = new Bank();
			
			Account wojtek = new Account ("Wojtek");
			Account weronika = new Account ("Weronika");
			Account monika = new Account ("Monika");
			
			bank.addAccount(wojtek);
			bank.addAccount(weronika);
			bank.addAccount(monika);
			
			bank.income(monika, 80000);
			bank.income(monika, 200);
			bank.income(monika, 3100);
			
			bank.transfer(monika, weronika, 1000);
			bank.transfer(weronika, wojtek, 20000);
			bank.transfer(wojtek, monika, 1200);
		}
}