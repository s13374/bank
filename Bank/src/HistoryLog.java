import java.util.Calendar;
import java.util.Date;

public class HistoryLog {

		private Date dateOfOperation;
		private String title;
		private Operation operation;
		
		public HistoryLog (String title, Operation operation) {
			this.title = title;
			this.operation = operation;
			dateOfOperation = new Date (((Calendar.getInstance()).getTimeInMillis()));
		}
}