import java.util.ArrayList;
import java.util.List;

public class History {
	
	private List <HistoryLog> historyLogs;
	
	public History () {
		historyLogs = new ArrayList ();
	}
	
	public void log (HistoryLog historyLog) {
		historyLogs.add(historyLog);	
	}
}