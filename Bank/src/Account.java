public class Account {

		private String number;
		private History history;
		private double amount;
		
		public Account () {
			history = new History();
		}
		public Account (String number) {
			this.number = number;
			history = new History();
		}
		
		public void add (double amount) {
			this.amount += amount;
		}
		
		public void subtact (double amount) {
			this.amount -= amount;
			
		}
		
		public void doOperation (Operation operation) {
			operation.execute(this);
		}		
		
		public String getNumber() { return number; }
		public void setNumber(String number) { this.number = number; }
		
		public History getHistory() { return history; }
		public void setHistory(History history) { this.history = history; }
		
		public double getAmount() { return amount; }
		public void setAmount(double amount) { this.amount = amount; }
		
		
}