public class Income extends Operation {
	
	private Account account;
	private double amount;
	
	public Income (double amount) {
		this.amount = amount;
	}
	
	public void execute (Account account) {
		System.out.println("[" + account.getNumber() + "] otrzymuje przelew w wysokosci " + amount + " zl.");
		account.add(amount);
	}
}