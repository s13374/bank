import java.util.ArrayList;
import java.util.List;

public class Bank {	
	
	private List <Account> accounts;
	
	public Bank () {
		accounts = new ArrayList ();
	}
	
	public void income (Account account, double amount) {
		Operation operation = new Income (amount);
		account.doOperation(operation);
		account.getHistory().log (new HistoryLog ("income", operation));
	}
	
	public void transfer (Account accountFrom, Account accountTo, double amount) {
		Operation operation = new Transfer (accountTo, amount);
		accountFrom.doOperation(operation);
		accountFrom.getHistory().log(new HistoryLog ("transfer", operation));
		accountTo.getHistory().log(new HistoryLog ("transfer", operation));
	}
	
	public void addAccount (Account account) {
		accounts.add(account);
	}
	
	public void removeAccount (Account account) {
		accounts.remove(account);
	}
	
	public List<Account> getAccounts() { return accounts; }
	public void setAccounts(List<Account> accounts) { this.accounts = accounts; }	
}